@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <!-- используем компонент router-link для навигации -->
            <!-- входной параметр `to` определяет URL для перехода -->
            <!-- `<router-link>` по умолчанию отображается тегом `<a>` -->
            <router-link to="/">Home</router-link>
            <router-link to="/foo">Foo</router-link>
            <router-link to="/bar">Bar</router-link>
            <router-link to="/example">Example</router-link>
            <router-link to="/chat">Chat</router-link>
            <router-link to="/im">Messages</router-link>
        </div>
        <div class="col-md-12">
            <!-- отображаем тут компонент, для которого совпадает маршрут -->
            <router-view></router-view>

        </div>
    </div>
</div>
@endsection
