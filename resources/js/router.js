//import Vue from 'vue'
import VueRouter from 'vue-router';

//const Demo = () => import('./modules/Demo'); // Так если папка с Index.vue // ? ленивая подгрузка

import ExampleComponent from './components/ExampleComponent.vue';
import MyComponent from './components/MyComponent.vue';
import ChatBox from './components/ChatBox.vue';
// import PageNotFound from './components/PageNotFound.vue';
import Chat from './components/chat';

const Foo = { template: '<div>foo</div>' }
const Bar = { template: '<div>bar</div>' }

Vue.use(VueRouter);

export default new VueRouter({
    //mode: 'history',
    routes: [
        {
            path: '/',
            component: MyComponent,
        },
        {
            path: '/foo',
            component: Foo
        },
        {
            path: '/bar',
            component: Bar
        },
        {
            path: '/example',
            component: ExampleComponent
        },
        {
            path: '/chat',
            component: ChatBox
        },
        {
            path: '/im',
            component: Chat
        },
        // {
        //     path: '*',
        //     component: PageNotFound
        // }

    ]
})
