import * as api from '../../../api/chat.js'

export default {
  getAllMessages: ({commit}) => {

    console.log('chat/actions.js:getAllMessages');

    api.getAllMessages(messages => {
      commit('receiveAll', messages)
    })
  },
  sendMessage: ({commit}, payload) => {
    api.createMessage(payload, message => {
      commit('receiveMessage', message)
    })
  },
  switchThread: ({commit}, payload) => {
    commit('switchThread', payload)
  }
}
