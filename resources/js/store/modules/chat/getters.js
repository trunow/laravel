export default {


  // Всегда возвращает URI со слэшем на конце.
  // slashedUri: () => uri => {
  //   //   return uri.slice(-1) === '/' ? uri : uri + '/';
  //   // },

  // getTodoById: state => id => {
  //   return state.todos.find(todo => todo.id === id);
  // },

  foo: () => {
    return 'boo'
  },
  threads: state => state.threads,
  currentThread: state => {
    console.log('getter currentThread');
    return state.currentThreadID
      ? state.threads[state.currentThreadID]
      : {}
  },
  currentMessages: (state, getters) => {
    const thread = getters.currentThread//(state)
    return thread.messages
      ? thread.messages.map(id => state.messages[id])
      : []
  },
  unreadCount: ({ threads }) => {
    return Object.keys(threads).reduce((count, id) => {
      return threads[id].lastMessage.isRead ? count : count + 1
    }, 0)
  },
  sortedMessages: (state, getters) => {
    const messages = getters.currentMessages
    return messages.slice().sort((a, b) => a.timestamp - b.timestamp)
  }
}
