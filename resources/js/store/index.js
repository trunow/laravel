//import Vue from 'vue';
import Vuex from 'vuex';

// Глобальное хранилище.
import state from './state';
import mutations from './mutations';
import actions from './actions';
import getters from './getters';

// Хранилище модулей.
import modules from './modules';

Vue.use(Vuex);

export default new Vuex.Store({
    state, mutations, actions, getters, modules,
});
