import axios from 'axios';

export default {
    saveData ({ commit }, data) {
        axios
            .post('/foochange', data) // TODO save pageData
            .then(function(res){

                console.log('saveData', res.data);
                commit('setData', res.data);


            })
            .catch(function(err){
                console.error(err);
            });
    },
}
