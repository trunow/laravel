import Vue from "vue";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const meta_user = document.head.querySelector('meta[name="user-id"]');
window.user_id = 0;

if (meta_user) {

    window.user_id = meta_user.content;

    console.log('auth user ' + window.user_id);

    // Echo.private('App.User.' + user_id)
        // .notification(function (notification) {
        //     //console.log('App.Models.User.', notification);
        //     if (notification.data) {
        //         alert(notification.data);
        //         console.log(notification.data);
        //     }
        //     else console.log('notification without data');
        // })

    // Echo.channel('foo/bar')
    //     .listen('FooChange', (e) => {
    //         console.log('app.js: Echo.channel foo/bar listen FooChange!', e);
    //     });

    Echo.channel('public')
        .listen('MessageSent', (e) => {
            console.log('app.js: Echo.channel public listen MessageSent!', e);
        });
}
else {
    console.log('no auth user');
}

Vue.filter('time', timestamp => {
    var midnight = new Date().setHours(0,0,0,0), diff = timestamp - midnight;

    console.log(timestamp, midnight, diff, Date.now());

    return diff >= 0
        ? new Date(timestamp).toLocaleTimeString()
        : new Date(timestamp).toISOString().slice(0, 10).replace(/[^\d]/g, '/')
})

// const store = require('./store').default;
// const router = require('./router').default;

// const app = new Vue({
//     el: '#app',
//     store,
//     router
// });

const app = new Vue({
    store: require('./store').default,
    router: require('./router').default
}).$mount('#app');

