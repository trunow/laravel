<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('/foochange', function () {
    $request = \Request::input();
    $request['time'] = time();
    broadcast(new \App\Events\FooChange($request, Auth()->user()));
    //event(new \App\Events\FooChange($request, Auth()->user()));
    //\Log::info('foochange ' . json_encode($request, JSON_UNESCAPED_UNICODE));
    return json_encode($request, JSON_UNESCAPED_UNICODE);
});
